package br.edu.catolicato.viwork.modelo;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luídne on 31/10/2015.
 */
public class PostagemServicoREST {

    private static final String HOST = "http://192.168.56.1:8084/Viwork";
    private static final String RESOURCE_TODOS = "/api/postagem";

    OkHttpClient okHttpClient;
    ObjectMapper objectMapper;

    public PostagemServicoREST() {
        this.objectMapper = new ObjectMapper();
        this.okHttpClient = new OkHttpClient();
    }

    public List<Postagem> getTodos() throws IOException {
        Request request = new Request.Builder()
                .url(HOST + RESOURCE_TODOS)
                .build();

        Log.i(PostagemServicoREST.class.getName(), request.httpUrl().toString());
        Response response = okHttpClient.newCall(request).execute();

        return this.objectMapper.readValue(response.body().string(), TypeFactory.defaultInstance()
                .constructCollectionType(ArrayList.class, Postagem.class));
    }
}
