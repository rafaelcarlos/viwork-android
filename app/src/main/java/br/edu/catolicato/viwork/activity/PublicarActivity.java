package br.edu.catolicato.viwork.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import br.edu.catolicato.viwork.R;
import mehdi.sakout.fancybuttons.FancyButton;

public class PublicarActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private EditText legenda;
    private FancyButton btnPublicar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicar);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        legenda = (EditText) findViewById(R.id.editText);
        btnPublicar = (FancyButton) findViewById(R.id.btnPublicar);
        btnPublicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Publicar().execute();
            }
        });
    }

    class Publicar extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btnPublicar.setEnabled(false);
            btnPublicar.setText("Enviando...");

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            btnPublicar.setLayoutParams(layoutParams);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            /*PostagensWebService postagensWebService = new PostagensWebService();

            return postagensWebService.addPostagem(idUsuario, byteArray, legenda.getText().toString());*/
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            btnPublicar.setText("");
            if(result) {
                setResult(Activity.RESULT_OK, null);
                finish();
            } else {
                Toast.makeText(PublicarActivity.this, "Houve um erro. Tente novamente.", Toast.LENGTH_LONG).show();
                btnPublicar.setEnabled(true);
            }
        }
    }
}
