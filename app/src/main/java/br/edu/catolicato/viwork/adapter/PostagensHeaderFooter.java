package br.edu.catolicato.viwork.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.edu.catolicato.viwork.R;
import br.edu.catolicato.viwork.modelo.Postagem;

/**
 * Created by Luídne on 14/08/2015.
 */
public class PostagensHeaderFooter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;

    //our items
    List<Postagem> items = new ArrayList<>();
    //headers
    List<View> headers = new ArrayList<>();
    //footers
    List<View> footers = new ArrayList<>();

    public static final int TYPE_HEADER = 111;
    public static final int TYPE_FOOTER = 222;
    public static final int TYPE_ITEM = 333;

    public PostagensHeaderFooter(Context context, List<Postagem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type) {
        //if our position is one of our items (this comes from getItemViewType(int position) below)
        if(type == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_postagem, viewGroup, false);
            return new PostagemViewHolder(view);
            //else we have a header/footer
        }else{
            //create a new framelayout, or inflate from a resource
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            //make sure it fills the space
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return new HeaderFooterViewHolder(frameLayout);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder vh, int position) {
        //check what type of view our position is
        if(position < headers.size()){
            View v = headers.get(position);
            //add our view to a header view and display it
            prepareHeaderFooter((HeaderFooterViewHolder) vh, v);
        }else if(position >= headers.size() + items.size()){
            View v = footers.get(position-items.size()-headers.size());
            //add oru view to a footer view and display it
            prepareHeaderFooter((HeaderFooterViewHolder) vh, v);
        }else {
            //it's one of our items, display as required
            prepareGeneric((PostagemViewHolder) vh, position - headers.size());
        }
    }

    @Override
    public int getItemCount() {
        //make sure the adapter knows to look for all our items, headers, and footers
        return headers.size() + items.size() + footers.size();
    }

    public int getItemsSize() {
        return items.size();
    }

    private void prepareHeaderFooter(HeaderFooterViewHolder vh, View view){
        //empty out our FrameLayout and replace with our header/footer
        vh.base.removeAllViews();
        vh.base.addView(view);
    }

    private void prepareGeneric(final PostagemViewHolder holder, int position){
        final Postagem postagem = items.get(position);

        holder.legenda.setText(postagem.getTexto());
        holder.progressBar.setVisibility(View.VISIBLE);
        holder.textViewFalha.setVisibility(View.GONE);
//        holder.data.setText(DateUtils.getRelativeTimeSpanString(postagem.getData().getTime(), Calendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS));
    }

    @Override
    public int getItemViewType(int position) {
        //check what type our position is, based on the assumption that the order is headers > items > footers
        if(position < headers.size()){
            return TYPE_HEADER;
        }else if(position >= headers.size() + items.size()){
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    //add a header to the adapter
    public void addHeader(View header){
        if(!headers.contains(header)){
            headers.add(header);
            //animate
            notifyItemInserted(headers.size()-1);
        }
    }

    //remove a header from the adapter
    public void removeHeader(View header){
        if(headers.contains(header)){
            //animate
            notifyItemRemoved(headers.indexOf(header));
            headers.remove(header);
        }
    }

    //add a footer to the adapter
    public void addFooter(View footer){
        if(!footers.contains(footer)){
            footers.add(footer);
            //animate
            notifyItemInserted(headers.size() + items.size() + footers.size() - 1);
        }
    }

    //remove a footer from the adapter
    public void removeFooter(View footer){
        if(footers.contains(footer)) {
            //animate
            notifyItemRemoved(headers.size()+items.size()+footers.indexOf(footer));
            footers.remove(footer);
        }
    }

    public Postagem getItem(int position) {
        return items.get(position - headers.size());
    }

    public void addItems(List<Postagem> lista) {
        this.items.addAll(lista);
        this.notifyDataSetChanged();
    }

    public void removeItems() {
        this.items.clear();
        this.notifyDataSetChanged();
    }

    public static class PostagemViewHolder extends RecyclerView.ViewHolder {
        public View itemView;

        public final TextView data;
        public final TextView legenda;
        public final ProgressBar progressBar;
        public final TextView textViewFalha;

        public PostagemViewHolder(final View itemView) {
            super(itemView);

            this.itemView = itemView;

            this.data = (TextView) itemView.findViewById(R.id.textViewData);
            this.legenda = (TextView) itemView.findViewById(R.id.textViewTexto);
            this.progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar2);
            this.textViewFalha = (TextView) itemView.findViewById(R.id.textViewFalha);
        }
    }

    //our header/footer RecyclerView.ViewHolder is just a FrameLayout
    public static class HeaderFooterViewHolder extends RecyclerView.ViewHolder{
        FrameLayout base;
        public HeaderFooterViewHolder(View itemView) {
            super(itemView);
            this.base = (FrameLayout) itemView;
        }
    }
}
